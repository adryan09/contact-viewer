import React, { FC, memo, useCallback } from 'react';

interface ModalPublicModel {
    readonly id: number;
    readonly onClick: (actionType: string) => (event: React.MouseEvent) => void;
    readonly onClose: () => void;
}

const Modal: FC<ModalPublicModel> = memo(function ModalComponent({ id, onClick, onClose }: ModalPublicModel){
    const handleClick = useCallback((type: string) => (event: React.MouseEvent) => {
        onClick(type)(event);
    }, [onClick]);

    const handleClose = useCallback(() => {
        onClose();
    }, [onClose]);
    
    
    return (
        <div className="modal">
            <div className='modal-content'>
                <button onClick={handleClick('Add')}>Add a new contact </button>
                <button onClick={handleClick('Edit')}>Edit contact</button>
                <button onClick={handleClick('Remove')}>Delete contact</button>
                <button onClick={handleClose}>Close</button>
            </div>
        </div>
    )
});

export default Modal;