import axios from "axios";
import { v4 as uuidv4 } from 'uuid';
import React, { FC, memo, useCallback, useContext, useEffect, useRef, useState } from "react";
import { ContactContext, ContactContextModel } from "../context/contacts-context";
 
interface ContactsPublicModel {
    readonly onContactItemClick: (id: number) => (event: React.MouseEvent)=> void;
}

const Contacts: FC<ContactsPublicModel> = memo(function ContactsComponent({ onContactItemClick }: ContactsPublicModel){
    const { storeFetchedContacts, contacts }: ContactContextModel = useContext(ContactContext) as ContactContextModel;
    const [error, setError] = useState<string>('');

    const fetchApi = useCallback(async () => {
        try {
            const { data } = await axios.get('https://61c32f169cfb8f0017a3e9f4.mockapi.io/api/v1/contacts');
            storeFetchedContacts(data);
        } catch (error) {
            setError('There was an error fetching the contacts')
        }
    }, [storeFetchedContacts, contacts]);

    const renderContacts = useCallback(() => {
        return contacts.contactIds.map((id: number) => {
            const { name, avatar } = contacts.contacts[id];
            return (
                <div 
                className='contacts'
                key={uuidv4()}
                onClick={onContactItemClick(id)}
                >
                    <div>{name}</div>
                    <img style={{
                        'maxWidth': '100%'
                    }} src={avatar}
                     />
                </div>
            )
        })
    }, [contacts]);


    useEffect(() =>  {
        let isMounted: boolean =  true;
        if (isMounted) {
            fetchApi();
        }

        return () => {
            isMounted = false;
        }
    }, []);

    if (error) {
        return (
            <div>
                {error}
            </div>
        )
    }
  
    return (
        <div className="contacts-wrapper">
            <div className="contacts-number">Number of contacts {contacts.contactIds.length}</div>
            {renderContacts()}
        </div>
    )
});

export default Contacts;