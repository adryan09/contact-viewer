import React, { FC, memo, useCallback, useContext, useMemo, useState } from 'react';
import { ContactContext, ContactContextModel } from '../context/contacts-context';
import ContactsForm from './contacts-form';
import Modal from './modal';

interface ModalWrapperPublicProps {
    readonly contactId: number | undefined;
    readonly showModal: boolean;
    readonly closeModal: () => void;
}

const ModalWrapper: FC<ModalWrapperPublicProps> = memo(function ModalWrapperComponent(
    { contactId, showModal, closeModal }: ModalWrapperPublicProps){
    const { removeContact }: ContactContextModel = useContext(ContactContext) as ContactContextModel;
    const [screenType, setScreenType] = useState<string>('');

    const handleModalFieldOnClick = useCallback((type: string) => (event: React.MouseEvent) => {
        if (type === 'Remove') {
            removeContact(contactId as number);
        }

        switch(type) {
          case 'Add': 
            setScreenType('Add');
            break;
          case 'Edit': 
            setScreenType('Edit');
            break;  
          default:
            return;
        }
    }, [screenType, setScreenType, removeContact, contactId]);

    const handleOnClose = useCallback(() => {
        closeModal();
    }, [closeModal]);
    
    const contactsForm = useMemo<JSX.Element | null>(() => {
        if (!screenType) {
            return null;
        }
        return (
            <ContactsForm  
                contactId={contactId}
                isAddMode={screenType === 'Add'}
                closeModal={handleOnClose}
            />
        )
    }, [screenType]);

    return (
        <div className='modal-wrapper'>
            {contactsForm}
            {!screenType && contactId && showModal && (
                <Modal 
                    onClick={handleModalFieldOnClick}
                    id={contactId}
                    onClose={handleOnClose}
                />
            )}
        </div>
    )
});

export default ModalWrapper;