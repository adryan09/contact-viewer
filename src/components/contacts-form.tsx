import React, { FC, memo, useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import validator from 'validator';
import Bem from 'bem-react-helper';
import { ContactContext, ContactContextModel } from '../context/contacts-context';
import { ContactModel, FormStateModel } from '../models/contacts-models';

interface ContactsFormPublicProps {
  readonly isAddMode: boolean;
  readonly contactId: number | undefined;
  readonly closeModal: () => void;
}

const initialValuesAddContactsMode: FormStateModel = {
  name: '',
  avatar: '',
  birthday: '',
  email: '',
  phone: '',
};

type FielsValidationModel = {
  readonly [key in keyof FormStateModel]: boolean;
};

type FielsErrors = {
  readonly [key: string]: string;
}
const INITIAL_VALIDATION_STATE = {
  avatar: false,
  birthday: false,
  email: false,
  name: false,
  phone: false,
};

const INITIAL_FIELS_ERROR_STATE = {
  avatar: '',
  birthday: '',
  email: '',
  name: '',
  phone: '',
}

const FIELDS_ERRORS_TEXT: FielsErrors = {
  name: 'Please enter a valid username',
  avatar: 'Please enter a valid avatar url',
  phone: 'Please enter a valid phone',
  birthday: 'Please enter a valid birthday',
  email: 'Please enter a valid email',
}

const ContactsForm: FC<ContactsFormPublicProps> = memo(function AddContactFormComponent(
  { 
    isAddMode,
    contactId,
    closeModal,
  }: ContactsFormPublicProps
  ){
 
    const { addContact, contacts, editContact }: ContactContextModel = useContext(ContactContext) as ContactContextModel;
    const initialValues = useMemo(() => {
      if (isAddMode) {
        return initialValuesAddContactsMode;
      } else {
        const contactDetails = contacts.contacts[contactId!];

        return contactDetails;
      }
    }, [isAddMode, contactId]);

    const [formFiels, setFormFields] = useState<FormStateModel>(initialValues); 
    const [fieldsValidationData, setFieldsValidationData] = useState<FielsValidationModel>(INITIAL_VALIDATION_STATE);
    const [diff, setDiff] = useState<Partial<FormStateModel>>({});
    const [enabledSubmit, setEnabledSubmit] = useState<boolean>(false);
    const prevCountRef = useRef<Partial<FormStateModel> | undefined>();
    const [errors, setErrors] = useState<FielsErrors>(INITIAL_FIELS_ERROR_STATE);


    useEffect(() => {
        /*
          validate fields on edit mode 
        */
        if (!isAddMode) {
          validateUserName();
          validateEmail();
          validateAvatar();
          validateBirthDay();
          validatePhone();
        }

        /*
          This. entire functionality is related to edit mode.In edit mode submit button will be enabled only if
          user tries to change at least one field within the form so that to avoid any usless PUT request 
        */
        const stateContatDetails: ContactModel = contacts.contacts[contactId as number];
        let difference: Partial<FormStateModel> = {};
        Object.keys(stateContatDetails).forEach((keyObject: string) => {
           const stateContactDetailValue = stateContatDetails[keyObject as keyof FormStateModel];
           const formContactDetailsValue = formFiels[keyObject as keyof FormStateModel];
      
           if (stateContactDetailValue !== formContactDetailsValue) {
             difference = {
                ...diff,
                [keyObject]: formContactDetailsValue,
              }
           }
        });
        
        if (Object.keys(difference).length) {
          prevCountRef.current = difference;
          setDiff(difference);
          setEnabledSubmit(true);
        } else if (!Object.keys(difference).length && prevCountRef.current) {
          setEnabledSubmit(false);
        }

    }, [formFiels, contactId, contacts, isAddMode]);

    const handleEvent = useCallback((fieldName: string) => (event:  React.ChangeEvent<HTMLInputElement>): void => {
      if (event.type === 'change') {
        setFormFields((prevState) => {
          return {
            ...prevState,
            [fieldName]: event.target.value,
          }
        });
      }

      if (fieldName === 'name') {
        validateUserName();
      }
      if (fieldName === 'email') {
        validateEmail();
      }

      if (fieldName === 'phone') {
        validatePhone();
      }

      if (fieldName === 'birthday') {
        validateBirthDay();
      }

      if (fieldName === 'avatar') {
        validateAvatar();
      }

    }, [setFormFields, formFiels]);

    const validateUserName = useCallback(() => {

        const username: string | undefined = formFiels.name;
        if (username && username.length > 5) {
          setFieldsValidationData((prevState) => ({ ...prevState, name: true }));
          if (errors.name) {
   
            setErrors((prevState) => ({...prevState, name: ''}));
          }
        } else {
          setErrors((prevState) => ({ ...prevState, name: FIELDS_ERRORS_TEXT.name }));
          setFieldsValidationData((prevState) => ({ ...prevState, name: false }));
        }
    }, [setFieldsValidationData, formFiels.name]);

    const validateBirthDay = useCallback(() => {
      const birthDay: string | undefined = formFiels.birthday;
      if (birthDay) {
        setFieldsValidationData((prevState) => ({ ...prevState, birthday: true }));
        if (errors.birthday) {
          setErrors((prevState) => ({...prevState, birthday: ''}));
        }
      } else {
        setErrors((prevState) => ({ ...prevState, birthday: FIELDS_ERRORS_TEXT.birthDay }));
        setFieldsValidationData((prevState) => {
          return { ...prevState, birthday: false }
        });
      }
    }, [setFieldsValidationData, formFiels.birthday]);

    const validateEmail = useCallback(()=> {
      const email: string | undefined = formFiels.email;
      if (validator.isEmail(email as string)) {
        setFieldsValidationData((prevState) => ({...prevState, email: true }));
        if (errors.email) {
          setErrors((prevState) => ({...prevState, email: ''}));
        }
      } else {
        setErrors((prevState) => ({ ...prevState, email: FIELDS_ERRORS_TEXT.email }));
        setFieldsValidationData((prevState) => ({...prevState, email: false }));
      }
    }, [setFieldsValidationData, formFiels.email]);

    const validatePhone = useCallback(()=> {
      const phone: string | undefined = formFiels.phone;
      if (validator.isMobilePhone(phone as string)) {
        setFieldsValidationData((prevState) => ({...prevState, phone: true }));
        if (errors.phone) {
          setErrors((prevState) => ({...prevState, phone: ''}));
        }
      } else {
        setErrors((prevState) => ({ ...prevState, phone: FIELDS_ERRORS_TEXT.phone }));
        setFieldsValidationData((prevState) => ({...prevState, phone: false }));
      }
    }, [setFieldsValidationData, formFiels.phone]);

    const validateAvatar = useCallback(()=> {
      const avatar: string | undefined = formFiels.avatar;
      if (validator.isURL(avatar as string)) {
        setFieldsValidationData((prevState) => ({...prevState, avatar: true }));
        if (errors.avatar) {
          setErrors((prevState) => ({...prevState, avatar: ''}));
        }
      } else {
        setErrors((prevState) => ({ ...prevState, avatar: FIELDS_ERRORS_TEXT.avatar }));
        setFieldsValidationData((prevState) => ({...prevState, avatar: false }));
      }
    }, [setFieldsValidationData, formFiels.avatar]);


    const handleSubmit = useCallback((event: React.FormEvent) => {
      event.preventDefault();
      const formIsValid: boolean = Object.values(fieldsValidationData).every((value) => value === true);
      
      if (formIsValid) {
        if (isAddMode) {
          addContact(formFiels);
        } else {
            if (Object.keys(diff).length) {
              editContact(diff as FormStateModel, contactId!);
            }
        }
      } else {
        let newErrors: FielsErrors = {};
        Object.keys(errors).forEach((key: string) => {
          if (errors[key as keyof FielsErrors] === '') {
            newErrors = {
              ...errors,
              ...newErrors,
              [key]: FIELDS_ERRORS_TEXT[key],
            }
          }
        });
        setErrors(newErrors)

      } 

    }, [fieldsValidationData, closeModal, setErrors, errors, isAddMode, addContact, editContact]); 
    console.log('form fields', fieldsValidationData);
    return (
        <form onSubmit={handleSubmit}>
        <fieldset>
           <label>
             <p>Name</p>
             <input name="name" value={formFiels.name} onBlur={handleEvent('name')} onChange={handleEvent('name')} />
              {errors.name && <div className='error-message'>{errors.name}</div>}
           </label> 
           <label>
             <p>Avatar</p>
             <input name="avatar" value={formFiels.avatar} onBlur={handleEvent('avatar')} onChange={handleEvent('avatar')} />
             {errors.avatar && <div className='error-message'>{errors.avatar}</div>}
            </label>
            <label>
             <p>Email</p>
             <input name="email" value={formFiels.email} onBlur={handleEvent('email')}  onChange={handleEvent('email')} />
             {errors.email && <div className='error-message'>{errors.email}</div>}
            </label>
            <label>
             <p>Phone</p>
             <input name="phone" value={formFiels.phone}  onBlur={handleEvent('phone')} onChange={handleEvent('phone')} />
             {errors.phone && <div className='error-message'>{errors.phone}</div>}
            </label>
            <label>
             <p>Birthday</p>
             <input name="birthday" type="date" value={formFiels.birthday} onBlur={handleEvent('birthday')}  onChange={handleEvent('birthday')} />
             {errors.birthday && <div className='error-message'>{errors.birthday}</div>}
            </label>
         </fieldset>
         <button className={Bem('Submit', {
           mods: { Disabled: !enabledSubmit }
         })} type="submit">Submit</button>
        </form>
    )
});

export default ContactsForm;